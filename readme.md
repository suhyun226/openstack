<1026>
  개요  
  실습
    Controller Node
      Keystone

<1027>
  docker -> VMware
  기존 docker 사용하다가 VMware 사용하면 문제 발생

  제어판 -> 프로그램 추가/삭제 -> Windows 기능 켜기/끄기
    컨테이너 
    하이퍼-V
    리눅스용 윈도우즈 하위 시스템
    가상 머신 플랫폼
  재부팅 후에도 VMware가 동작하지 않은 경우 다음의 작업 추가 진행
   실행에서 msconfig 명령어 입력 -> 서비스 탭으로 이동 -> 서비스 명이 Hyper-V로 되어 있는 서비스 전부 중단 후 재부팅

  CentOS 7 만들기
    Controller Node
      CPU 2 core
      Memory 8 GB
      HDD 100GB

      NIC 2개
        - ens33(NAT) : 192.168.80.10           -> Managed
        - ens34(Bridge) : 192.168.0.100 ~ 125  -> Provider(서비스 제공용)
                                    130 ~ 155
                                    160 ~ 185
                                    190 ~ 215
                                    220 ~ 245
  
  3번 PC
  
  실습
  https://docs.openstack.org/install-guide/index.html
  OneNote(실습)
  
  Glance 설명
  
<1028>
  Nova Service 개요 및 구성 실습
  Controller node에 Compute node같이 구성
  